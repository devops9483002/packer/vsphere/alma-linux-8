packer {
  required_version = ">= 1.9.4"

  required_plugins {
    vsphere = {
      version = "~> 1"
      source  = "github.com/hashicorp/vsphere"
    }

    ansible = {
      version = "~> 1"
      source  = "github.com/hashicorp/ansible"
    }
  }
}

locals {
  data_source_command = "inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ks.cfg"
  data_source_content = {
    "/ks.cfg" = templatefile("${abspath(path.root)}/data/ks.pkrtpl.hcl", {
      vm_username   = var.vm_username
      build_ssh_key = file(var.public_ssh_key_path)
    })
  }
}

source "vsphere-iso" "almalinux8" {
  // vSphere Settings
  vcenter_server      = var.vsphere_endpoint
  username            = var.vsphere_username
  password            = var.vsphere_password
  insecure_connection = var.vsphere_insecure_connection
  datacenter          = var.vsphere_datacenter
  cluster             = var.vsphere_cluster
  host                = var.vsphere_host
  datastore           = var.vsphere_datastore
  folder              = var.vsphere_folder_path

  // Virtual Machine Settings
  vm_name              = var.vm_name
  guest_os_type        = "rhel8_64Guest"
  firmware             = "efi-secure"
  disk_controller_type = ["pvscsi"]
  CPUs                 = var.vm_cpu
  RAM                  = var.vm_ram

  // Template Settings
  convert_to_template = var.convert_to_template

  storage {
    disk_size             = var.vm_disk_size
    disk_thin_provisioned = true
  }

  network_adapters {
    network_card = "vmxnet3"
  }

  // ISO Settings
  iso_checksum = "sha256:f4d536fd1512fdf1936a8305454239bdcc9020f1b633d0ef37f1ff34a396b8d5"
  iso_url      = "https://repo.almalinux.org/almalinux/8.9/isos/x86_64/AlmaLinux-8.9-x86_64-dvd.iso"

  // Boot and Provisioning Settings
  http_content  = local.data_source_content
  http_ip       = var.http_ip
  http_port_min = var.http_port
  http_port_max = var.http_port
  boot_order    = "disk,cdrom"
  boot_wait     = "2s"
  boot_command = [
    "<up>",
    "e",
    "<down><down><end><wait>",
    " ${local.data_source_command}",
    "<enter><wait><leftCtrlOn>x<leftCtrlOff>"
  ]

  // Communicator Settings
  ssh_username         = var.vm_username
  ssh_private_key_file = var.private_ssh_key_path
}

build {
  sources = ["source.vsphere-iso.almalinux8"]

  provisioner "ansible" {
    user          = var.vm_username
    playbook_file = "${path.root}/ansible/configure-template.yml"

    ansible_env_vars = [
      "ANSIBLE_CONFIG=${path.root}/ansible/ansible.cfg"
    ]
  }
}
