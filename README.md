# Packer vSphere Alma Linux 8 Template

## Building Template

### Using Git Submodule

1. Add this repository as a submodule to your project.

2. Copy the `variables.pkrvars.hcl.example` file to `variables.pkrvars.hcl` one level above the submodule directory.

    Example:

    ```bash
    TODO: Add example tree
    ```

3. Edit the `variables.pkrvars.hcl` file and set the variables to the desired values.

4. Initialize Packer.

    ```bash
    packer init alma-linux-8/.
    ```

    Packer will host the Kickstart file on port `8792` by default. If running on a RHEL OS, make sure to allow port `8792` on the firewall for Kickstart file to be hosted. If you would like to change the port, add the `http_port` variable in the `variables.pkrvars.hcl` file.

    ```bash
    sudo firewall-cmd --permanent --add-port=8792/tcp
    sudo firewall-cmd --reload
    ```

5. Build the template using the `variables.pkrvars.hcl` file.

    ```bash
    packer build -var-file variables.pkrvars.hcl -force alma-linux-8/.
    ```
