// vSphere Variables

variable "vsphere_endpoint" {
  type = string
}

variable "vsphere_insecure_connection" {
  type    = bool
  default = true
}

variable "vsphere_username" {
  type = string
}

variable "vsphere_password" {
  type = string
}

variable "vsphere_datacenter" {
  type = string
}

variable "vsphere_cluster" {
  type = string
}

variable "vsphere_host" {
  type = string
}

variable "vsphere_network" {
  type = string
}

variable "vsphere_datastore" {
  type = string
}

variable "vsphere_folder_path" {
  type        = string
  description = "The folder to create the VM in. i.e. /example-folder"
}

variable "convert_to_template" {
  type        = bool
  description = "Convert the VM to a template after build"
  default     = true
}

// Virtual Machine Variables

variable "vm_name" {
  type        = string
  description = "The name of the virtual machine"
}

variable "vm_username" {
  type        = string
  description = "The username to use for the build"
}

variable "vm_cpu" {
  type        = number
  description = "The number of CPUs to use for the build"
  default     = 2
}

variable "vm_ram" {
  type        = number
  description = "The amount of memory to use for the build"
  default     = 2048
}

variable "vm_disk_size" {
  type        = number
  description = "The size of the disk to use for the build"
  default     = 20480
}

variable "public_ssh_key_path" {
  type        = string
  description = "The path to the public SSH key to use for the build"
}

variable "private_ssh_key_path" {
  type        = string
  description = "The path to the private SSH key to use for the build"
}

// Packer Variables

variable "http_port" {
  type        = number
  description = "The port to host the kickstart file on"
  default     = 8792
}

variable "http_ip" {
  type        = string
  description = "The IP address to host the kickstart file on"
  default     = null
}
